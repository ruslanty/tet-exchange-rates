# Exchange rates

#### Requirements:
+ PHP 7.2 or higher
+ MySQL 5.6 or higher
+ Composer (for installation)
+ npm (for installation)

#### Firstly, you need to create .env file:
```shell script
cp .env.example .env
```

#### Change your database connection settings in .env file:
```shell script
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=tet
DB_USERNAME=root
DB_PASSWORD=secret
```

#### Create a new schema (`DB_DATABASE`) in MySQL:
```sql
create schema tet;
```

#### Run composer to install vendors:
```shell script
composer install
```
It also runs:
* database migrations
* frontend compiling
* command to retrieve exchange rates
* web application on localhost:8000

#### To start running scheduled tasks:
```shell script
php artisan schedule:run
```
More info: https://laravel.com/docs/7.x/scheduling

#### To manually start exchange rates updating:
```shell script
php artisan exchange_rates:update  
```
