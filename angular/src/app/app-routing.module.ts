import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ExchangeRatesComponent} from './exchange-rates/exchange-rates.component';
import {ExchangeRateHistoryComponent} from './exchange-rate-history/exchange-rate-history.component';

const routes: Routes = [
    {path: '', redirectTo: '/exchange-rates', pathMatch: 'full'},
    {path: 'exchange-rates', component: ExchangeRatesComponent},
    {path: 'exchange-rate-history/:id', component: ExchangeRateHistoryComponent},
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRoutingModule {
}
