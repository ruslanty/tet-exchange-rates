export interface ExchangeRateHistory {
    rate: string;
    date: string;
}
