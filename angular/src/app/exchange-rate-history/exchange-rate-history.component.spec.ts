import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExchangeRateHistoryComponent } from './exchange-rate-history.component';

describe('ExchangeRateHistoryComponent', () => {
  let component: ExchangeRateHistoryComponent;
  let fixture: ComponentFixture<ExchangeRateHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExchangeRateHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExchangeRateHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
