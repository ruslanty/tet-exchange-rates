import {Component, OnInit} from '@angular/core';
import {ExchangeRateService} from '../exchange-rate.service';
import {ExchangeRateHistory} from '../exchange-rate-history';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';

@Component({
    selector: 'app-exchange-rate-history',
    templateUrl: './exchange-rate-history.component.html',
    styleUrls: ['./exchange-rate-history.component.css']
})

export class ExchangeRateHistoryComponent implements OnInit {
    exchangeRateCurrency: string;
    exchangeRateHistory: ExchangeRateHistory[];

    constructor(private route: ActivatedRoute, private exchangeRateService: ExchangeRateService, private location: Location) {
    }

    ngOnInit(): void {
        this.getExchangeRateHistory();
    }

    getExchangeRateHistory(): void {
        const id = +this.route.snapshot.paramMap.get('id');

        this.exchangeRateService.getExchangeRateHistory(id)
            .subscribe((response) => {
                this.exchangeRateCurrency = response.data.currency;
                this.exchangeRateHistory = response.data.history;
            });
    }

    goBack(): void {
        this.location.back();
    }
}
