import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable({
    providedIn: 'root',
})

export class ExchangeRateService {
    constructor(private http: HttpClient) {
    }

    getExchangeRates(page: number): Observable<any> {
        const uri = `/api/v1/exchange-rates/list?page=${page}`;

        return this.http.get<any>(uri);
    }

    getExchangeRateHistory(id: number): Observable<any> {
        const uri = `/api/v1/exchange-rates/${id}/history`;

        return this.http.get<any>(uri);
    }
}
