import {Component, OnInit} from '@angular/core';
import {ExchangeRate} from '../exchange-rate';
import {ExchangeRateService} from '../exchange-rate.service';

@Component({
    selector: 'app-exchange-rates',
    templateUrl: './exchange-rates.component.html',
    styleUrls: ['./exchange-rates.component.css']
})

export class ExchangeRatesComponent implements OnInit {
    exchangeRates: ExchangeRate[];

    currentPage: number;
    pageSize: number;
    pageCount: number;

    constructor(private exchangeRateService: ExchangeRateService) {
    }

    ngOnInit(): void {
        this.getExchangeRates();
    }

    getExchangeRates(page: number = 1): void {
        this.exchangeRateService.getExchangeRates(page)
            .subscribe((response) => {
                this.currentPage = response.current_page;
                this.pageSize = response.per_page;
                this.pageCount = response.total;

                this.exchangeRates = response.data;
            });
    }

    onPageChange(page: number): void {
        this.getExchangeRates(page);
    }
}
