<?php

declare(strict_types=1);

namespace App\Console\Commands;

use App\Services\BankService;
use App\Services\ExchangeRateService;
use Illuminate\Console\Command;

final class UpdateExchangeRates extends Command
{
    protected $signature = 'exchange_rates:update';

    protected $description = 'Update exchange rates';

    public function handle(BankService $bankService, ExchangeRateService $exchangeRateService): void
    {
        $exchangeRates = $bankService->getRatesForCurrentDate();

        $exchangeRateService->store($exchangeRates);
    }
}
