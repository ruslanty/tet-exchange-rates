<?php

declare(strict_types=1);

namespace App\Events;

use App\Models\ExchangeRate;

final class ExchangeRateSaved extends Event
{
    private $exchangeRate;

    public function __construct(ExchangeRate $exchangeRate)
    {
        $this->exchangeRate = $exchangeRate;
    }

    public function getExchangeRate(): ExchangeRate
    {
        return $this->exchangeRate;
    }
}
