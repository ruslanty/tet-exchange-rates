<?php

declare(strict_types=1);

namespace App\Exceptions;

use Exception;

final class ExchangeRateNotFoundException extends Exception
{
    public function __construct()
    {
        parent::__construct('Exchange rate not found.');
    }
}
