<?php

declare(strict_types=1);

namespace App\Exceptions;

use ErrorException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Throwable $exception
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        if ($request->is('api/*')) {
            return $this->renderApiException($exception);
        }

        return parent::render($request, $exception);
    }

    private function renderApiException(Throwable $exception)
    {
        [$code, $message, $data] = $this->prepareExceptionData($exception);

        return response()->json([
            'success' => false,
            'message' => $message,
            'data' => $data,
        ], $code);
    }

    private function prepareExceptionData(Throwable $exception): array
    {
        if ($exception instanceof ValidationException) {
            return [Response::HTTP_UNPROCESSABLE_ENTITY, 'Validation error.', $exception->errors()];
        }

        if ($exception instanceof QueryException) {
            return [Response::HTTP_CONFLICT, 'Database error.', $exception->errorInfo];
        }

        if ($exception instanceof ErrorException) {
            return [Response::HTTP_EXPECTATION_FAILED, 'Internal error.', $this->parseException($exception)];
        }

        if ($exception instanceof ModelNotFoundException) {
            return [Response::HTTP_NOT_FOUND, 'Record not found.', $this->parseException($exception)];
        }

        if ($exception instanceof AuthorizationException) {
            return [Response::HTTP_UNAUTHORIZED, 'This action is unauthorized.', $this->parseException($exception)];
        }

        if ($exception instanceof NotFoundHttpException) {
            return [Response::HTTP_NOT_FOUND, 'Page not found.', $this->parseException($exception)];
        }

        return [Response::HTTP_BAD_REQUEST, 'Bad request.', $this->parseException($exception)];
    }

    private function parseException(Throwable $exception): array
    {
        $debug = env('APP_DEBUG', true);

        if (!$debug) {
            return [];
        }

        return [
            'exception' => get_class($exception),
            'message' => $exception->getMessage(),
            'trace' => $exception->getTraceAsString(),
        ];
    }
}
