<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Resources\ExchangeRateResource;
use App\Models\ExchangeRate;

final class ExchangeRateHistory extends Controller
{
    public function __invoke(int $exchangeRateId)
    {
        $exchangeRate = ExchangeRate::findOrFail($exchangeRateId);
        $exchangeRateResource = new ExchangeRateResource($exchangeRate);

        return $this->successResponse($exchangeRateResource);
    }
}
