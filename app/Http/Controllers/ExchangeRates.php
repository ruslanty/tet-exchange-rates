<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\ExchangeRate;

final class ExchangeRates extends Controller
{
    public function __invoke()
    {
        $perPage = 20;
        $columns = [
            'id',
            'currency',
            'rate',
        ];

        $exchangeRates = ExchangeRate::paginate($perPage, $columns);

        return response()->json($exchangeRates);
    }
}
