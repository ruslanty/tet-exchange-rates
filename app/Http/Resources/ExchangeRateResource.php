<?php

declare(strict_types=1);

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

final class ExchangeRateResource extends JsonResource
{
    public function toArray($request): array
    {
        /** @var \App\Models\ExchangeRate $this */
        return [
            'id' => $this->id,
            'currency' => $this->currency,
            'history' => HistoricalExchangeRateResource::collection($this->historical()->latest()->get()),
        ];
    }
}
