<?php

declare(strict_types=1);

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

final class HistoricalExchangeRateResource extends JsonResource
{
    public function toArray($request): array
    {
        /** @var $this \App\Models\HistoricalExchangeRate */
        return [
            'rate' => $this->rate,
            'date' => $this->actual_date,
        ];
    }
}
