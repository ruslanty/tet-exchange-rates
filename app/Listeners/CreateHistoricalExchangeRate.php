<?php

declare(strict_types=1);

namespace App\Listeners;

use App\Events\ExchangeRateSaved;

final class CreateHistoricalExchangeRate
{
    public function handle(ExchangeRateSaved $event): void
    {
        $exchangeRate = $event->getExchangeRate();

        $exchangeRate->historical()->create([
            'rate' => $exchangeRate->rate,
        ]);
    }
}
