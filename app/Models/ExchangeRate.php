<?php

declare(strict_types=1);

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class ExchangeRate
 *
 * @property int $id
 * @property string $currency
 * @property float $rate
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \App\Models\HistoricalExchangeRate[]|\Illuminate\Support\Collection $historical
 *
 * @method ExchangeRate updatedToday()
 *
 * @mixin \Illuminate\Database\Eloquent\Builder
 *
 * @package App\Models
 */
final class ExchangeRate extends Model
{
    protected $table = 'exchange_rates';

    protected $guarded = [];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    */

    public function historical(): HasMany
    {
        return $this->hasMany(HistoricalExchangeRate::class);
    }

    /*
    |--------------------------------------------------------------------------
    | Scopes
    |--------------------------------------------------------------------------
    */

    public function scopeUpdatedToday(Builder $query): Builder
    {
        return $query->whereDate('updated_at', Carbon::today());
    }
}
