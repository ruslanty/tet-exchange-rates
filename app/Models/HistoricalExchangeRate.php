<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class HistoricalExchangeRate
 *
 * @property int $id
 * @property int $exchange_rate_id
 * @property float $rate
 * @property \Carbon\Carbon $created_at
 *
 * @property string $actual_date
 *
 * @property \App\Models\ExchangeRate $currency
 *
 * @mixin \Illuminate\Database\Eloquent\Builder
 *
 * @package App\Models
 */
final class HistoricalExchangeRate extends Model
{
    protected $table = 'historical_exchange_rates';

    protected $guarded = [];

    protected $dates = [
        'created_at',
    ];

    public const UPDATED_AT = null;

    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    */

    public function getActualDateAttribute(): string
    {
        return $this->created_at->toDateString();
    }

    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    */

    public function currency(): BelongsTo
    {
        return $this->belongsTo(ExchangeRate::class);
    }
}
