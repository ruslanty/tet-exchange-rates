<?php

declare(strict_types=1);

namespace App\Observers;

use App\Events\ExchangeRateSaved;
use App\Models\ExchangeRate;

final class ExchangeRateObserver
{
    public function saved(ExchangeRate $exchangeRate): void
    {
        event(new ExchangeRateSaved($exchangeRate));
    }
}
