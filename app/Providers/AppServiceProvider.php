<?php

declare(strict_types=1);

namespace App\Providers;

use App\Models\ExchangeRate;
use App\Observers\ExchangeRateObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        ExchangeRate::observe(ExchangeRateObserver::class);
    }
}
