<?php

declare(strict_types=1);

namespace App\Providers;

use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        \App\Events\ExchangeRateSaved::class => [
            \App\Listeners\CreateHistoricalExchangeRate::class,
        ],
    ];
}
