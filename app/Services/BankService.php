<?php

declare(strict_types=1);

namespace App\Services;

use App\Exceptions\ExchangeRateNotFoundException;
use App\Facades\Feeds;

final class BankService
{
    public function getRatesForCurrentDate(): array
    {
        $uri = config('bank.request_api_uri');
        $feed = Feeds::make($uri);
        $items = $feed->get_items();
        $exchangeRates = [];

        $item = end($items);

        if (!$item) {
            throw new ExchangeRateNotFoundException();
        }

        $description = $item->get_description();
        $pattern = '~\w+\s[\d.]+~';

        if (preg_match_all($pattern, $description, $matches)) {
            foreach ($matches[0] as $match) {
                [$currency, $rate] = explode(' ', $match);

                $exchangeRates[$currency] = $rate;
            }
        }

        return $exchangeRates;
    }
}
