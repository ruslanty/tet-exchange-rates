<?php

declare(strict_types=1);

namespace App\Services;

use App\Exceptions\ExchangeRatesAlreadyUpdatedException;
use App\Models\ExchangeRate;
use Illuminate\Support\Facades\DB;

final class ExchangeRateService
{
    public function store(array $exchangeRates): void
    {
        if (ExchangeRate::updatedToday()->exists()) {
            throw new ExchangeRatesAlreadyUpdatedException();
        }

        DB::transaction(function () use ($exchangeRates) {
            foreach ($exchangeRates as $currency => $rate) {
                $exchangeRate = ExchangeRate::firstOrNew([
                    'currency' => $currency,
                ]);

                $exchangeRate->rate = $rate;
                $exchangeRate->touch();
            }
        });
    }
}
