#!/bin/sh

cd angular/ || exit

npm install -g @angular/cli

npm install

ng build --prod

cd ../

cp angular/dist/tet-angular/* public

mv public/index.html resources/views/app.blade.php

php artisan migrate

php artisan exchange_rates:update

php -S localhost:8000 -t public
