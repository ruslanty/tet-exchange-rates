<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoricalExchangeRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historical_exchange_rates', function (Blueprint $table) {
            $table->id();

            $table->foreignId('exchange_rate_id');
            $table->unsignedDecimal('rate', 16, 8);

            $table->timestamp('created_at')->nullable();

            $table->foreign('exchange_rate_id')
                ->references('id')
                ->on('exchange_rates')
                ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historical_exchange_rates');
    }
}
