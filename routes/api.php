<?php

declare(strict_types=1);

use App\Http\Controllers\ExchangeRateHistory;
use App\Http\Controllers\ExchangeRates;
use Laravel\Lumen\Routing\Router;

$app->router->group([
    'prefix' => 'api',
], static function (Router $router) {
    $router->group([
        'prefix' => 'v1',
    ], static function (Router $router) {
        $router->get('/', static function () use ($router) {
            return $router->app->version();
        });

        $router->group([
            'prefix' => 'exchange-rates',
        ], static function (Router $router) {
            $router->get('list', ExchangeRates::class);
            $router->get('{exchangeRateId:\d+}/history', ExchangeRateHistory::class);
        });
    });
});
